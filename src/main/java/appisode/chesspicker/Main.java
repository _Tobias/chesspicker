package appisode.chesspicker;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

import java.util.Random;


public class Main extends ActionBarActivity implements Animator.AnimatorListener {
    ImageView blackKing;
    ImageView whiteKing;
    TextView pickNow;
    int blackRotation;
    int whiteRotation;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        blackKing = (ImageView)findViewById(R.id.black_king);
        whiteKing = (ImageView)findViewById(R.id.white_king);
        pickNow = (TextView)findViewById(R.id.choose);
        if(savedInstanceState != null && savedInstanceState.containsKey(BLACK)) {
            ObjectAnimator.ofFloat(savedInstanceState.getBoolean(BLACK) ? whiteKing : blackKing, "rotationX", 0, 180)
                    .setDuration(0).start();
            blackRotation = savedInstanceState.getInt(BLACK_ROTATION);
            whiteRotation = savedInstanceState.getInt(WHITE_ROTATION);
        }

        AdView adView = (AdView)this.findViewById(R.id.ad_view);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("084BCF0DDFC684389CBF00EB90DD8C84")
                .build();
        adView.loadAd(adRequest);
    }

    static String BLACK = "black";
    static String BLACK_ROTATION = "blackr";
    static String WHITE_ROTATION = "whiter";
    public void onSaveInstanceState(Bundle out) {
        if(blackRotation != whiteRotation) {
            out.putBoolean(BLACK, blackRotation == 0);
            out.putInt(BLACK_ROTATION, blackRotation);
            out.putInt(WHITE_ROTATION, whiteRotation);
        }
    }

    boolean busy;
    public void pickNow(View v) {
        if(busy)
            return;

        Random random = new Random();
        boolean black = random.nextInt(100) > 49;
        boolean turn = random.nextInt(100) > 49;
        int blackRounds = (random.nextInt(3)+2) * (turn ? 1 : -1);
        int whiteRounds = (random.nextInt(3)+2) * (turn ? -1 : 1);

        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(blackKing, "rotationX", blackRotation, (black ? 0 : 180) + blackRounds * 360),
                ObjectAnimator.ofFloat(whiteKing, "rotationX", whiteRotation, (!black ? 0 : 180) + whiteRounds * 360)
        );
        if(black) {
            blackRotation = 0;
            whiteRotation = 180;
        }
        else {
            blackRotation = 180;
            whiteRotation = 0;
        }
        set.addListener(this);
        int durationMod = 0;
        durationMod = random.nextInt(500);
        if(turn)
            durationMod *= -1;
        set.setDuration(2000 + durationMod).start();
    }

    public void onAnimationStart(Animator animation) {
        pickNow.setEnabled(false);
        busy = true;
    }

    public void onAnimationEnd(Animator animation) {
        pickNow.setEnabled(true);
        busy = false;
    }

    public void onAnimationCancel(Animator animation) {}

    public void onAnimationRepeat(Animator animation) {}
}
